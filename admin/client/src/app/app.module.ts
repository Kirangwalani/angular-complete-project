import { ProductService } from './services/product.service';
import { NotificationService } from './services/notification.service';
import { UserGuard } from './shared/guard/user.guard';
import { AuthService } from './services/auth.service';
import { PageHeaderModule } from './shared/modules/page-header/page-header.module';
import { CommonModule } from '@angular/common';
import { HttpClient, HTTP_INTERCEPTORS ,HttpClientModule, } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';;
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { ToastrModule } from 'ngx-toastr';
import { UsersService } from './services/users.service';
import { LoginGuard } from './shared/guard/login.guard';
import {ConfirmationService} from 'primeng/api';


import 'hammerjs';
import { AuthInterceptorService } from './shared/guard/auth-interceptor.service';
// import { MatCardModule } from '@angular/material';
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        // MatCardModule,
        BrowserAnimationsModule,
        HttpClientModule,
   
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,PageHeaderModule,
        ToastrModule.forRoot()
    ],
    declarations: [AppComponent],
    providers: [
        ConfirmationService,
        AuthGuard,
        AuthService,
        LoginGuard,
         UserGuard,
         NotificationService,
         ProductService,UsersService,
        { provide: HTTP_INTERCEPTORS,useClass: AuthInterceptorService, multi:true}
        ],
    bootstrap: [AppComponent],
    // schemas: [
    //     CUSTOM_ELEMENTS_SCHEMA,
    //     NO_ERRORS_SCHEMA
    //   ]
})
export class AppModule {}
