import { NotificationService } from './../../../services/notification.service';
import { BlogService } from './../../../services/blog.service';
import { routerTransition } from './../../../router.animations';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss'],
  animations: [routerTransition()]
})
export class BlogListComponent implements OnInit {
  blogList: any[];
  constructor(
    private blog: BlogService,
    private toastr: NotificationService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {

    this.getBlogs();
  }
  getBlogs() {
    this.blog.listBlog().subscribe((res: any) => {
      this.blogList = res;
      console.log(res);

    });
  }

  delete(id){

    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.blog.deleteBlog(id).subscribe(res=>{
          if(res.code == 200){
            this.toastr.success("successfully deleted");
          }
          else{
            this.toastr.error("please try again")
          }
          this.getBlogs();
        })
    
      },
      reject: () => {
                }
  });

  }
}
