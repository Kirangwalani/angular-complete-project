import { BlogListComponent } from './blog-list/blog-list.component';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogsComponent } from './blogs.component';
import { EditBlogComponent } from './edit-blog/edit-blog.component';


const routes: Routes = [
  {
      path: '',
      component: BlogsComponent
  },
  {
    path:'list',
    component:BlogListComponent
  },
  {
    path:'edit/:id',
    component:EditBlogComponent
  }
]; 
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports:[RouterModule],
  declarations: [],

})
export class BlogRoutingModule { }
