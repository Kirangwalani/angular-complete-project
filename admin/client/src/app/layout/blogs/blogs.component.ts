import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { BlogService } from './../../services/blog.service';
import { NotificationService } from './../../services/notification.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from './../../router.animations';
import { Component, OnInit } from '@angular/core';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

declare var $: any;
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss'], animations: [routerTransition()]
})
export class BlogsComponent implements OnInit {
  blogForm: FormGroup;
  selectedFile: File;
  localUrl: any;


  constructor(
    private toastr: NotificationService,
    private blog: BlogService,
    private fb: FormBuilder
  ) { }


  ngOnInit() {
    this.blogForm = this.fb.group({
      title: new FormControl(''),
      description: new FormControl(''),
      about: new FormControl(''),
      file: null

    })
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.localUrl = (<FileReader>event.target).result;
      }

      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("file", file);
      this.blogForm.get('file').setValue(file);
    }


  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('title', this.blogForm.get('title').value);
    input.append('description', this.blogForm.get('description').value);
    input.append('description', this.blogForm.get('about').value);
    input.append('file', this.blogForm.get('file').value);
    return input;
  }

  addBlog() {
    const formModel = this.prepareSave();
    console.log("jcbnjedwnce", formModel);
    this.blog.addBlog(formModel).subscribe(res => {
      if (res.code == 404) {
        this.toastr.error(res.message)

      }
      else if (res.code == 200) {
        this.toastr.success("successfully added")

      }

    })


  }
}
