import { DataTableModule } from 'angular-6-datatable';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BlogRoutingModule } from './blog-routing.module';
import { PageHeaderModule } from './../../shared/modules/page-header/page-header.module';
import { BlogsComponent } from './blogs.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogListComponent } from './blog-list/blog-list.component';
import { FileSelectDirective } from 'ng2-file-upload';
import {TableModule} from 'primeng/table';
import { EditBlogComponent } from './edit-blog/edit-blog.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CalendarModule} from 'primeng/calendar';
import {EditorModule} from 'primeng/editor';
import { ConfirmDialogModule } from 'primeng/confirmdialog';


@NgModule({
  imports: [
    TableModule,
    CommonModule,
    PageHeaderModule,
    BlogRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    DataTableModule,CalendarModule,
    EditorModule,
    ConfirmDialogModule

  ],
  declarations: [
    BlogsComponent,
    BlogListComponent,
    FileSelectDirective,
    EditBlogComponent
    ]
})
export class BlogsModule { }
