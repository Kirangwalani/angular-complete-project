import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { routerTransition } from './../../../router.animations';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Component, OnInit } from '@angular/core';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { NotificationService } from '../../../services/notification.service';
import { BlogService } from '../../../services/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.scss'],
  animations:[routerTransition()]
})
export class EditBlogComponent implements OnInit {

  blogForm: FormGroup;
  selectedFile: File;
  localUrl: any;
  blogData:any;

  constructor(
    private toastr: NotificationService,
    private blog: BlogService,
    private fb: FormBuilder,
    private route :ActivatedRoute,
    private router :Router
  ) { }

  ngOnInit() {
    this.blogForm = this.fb.group({
      title: new FormControl(''),
      description: new FormControl(''),
      file: null

    })

    this.route.params.subscribe(params =>{
      this.blog.getBlogDetail(params.id).subscribe(res =>{
        this.blogData = res.data;

        this.blogForm.patchValue({
          title : this.blogData.title, 
          description : this.blogData.description,

        })
        this.localUrl =this.blogData.blogImage;
      })
    })
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.localUrl = (<FileReader>event.target).result;
      }

      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("file", file);
      this.blogForm.get('file').setValue(file);
    }


  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('title', this.blogForm.get('title').value);
    input.append('description', this.blogForm.get('description').value);
    input.append('file', this.blogForm.get('file').value);
    return input;
  }

  addBlog() {
    const formModel = this.prepareSave();
    console.log("jcbnjedwnce", formModel);
    this.route.params.subscribe(params =>{
    this.blog.editBlog(params.id,formModel).subscribe(res => {
      if (res.code == 404) {
        this.toastr.error(res.message)

      }
      else if (res.code == 200) {
        this.toastr.success("successfully added")
        this.router.navigate(['/layout/blog/list '])

      }

    })


  
  })
}
}
