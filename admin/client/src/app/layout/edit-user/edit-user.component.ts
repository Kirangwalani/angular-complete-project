import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from './../../services/users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { routerTransition } from './../../router.animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  animations: [routerTransition()]
})
export class EditUserComponent implements OnInit {

  userForm: FormGroup
  userData: any
  constructor(
    private user: UsersService,
    private toastr: ToastrService,
    private route: ActivatedRoute


  ) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "password": new FormControl(''),
      "role": new FormControl(''),
    });

    this.route.params.subscribe(params => {
      console.log(params)
      this.user.getUserDetails(params.id).subscribe(res => {
        console.log(res)
        this.userData = res.data;


        console.log(this.userData)
        this.userForm.patchValue({
          name: this.userData.name,
          email: this.userData.email,
          password: this.userData.password,
          role: this.userData.role

        })

      })

    });


  }

  onUpdateUser() {
    this.route.params.subscribe(params=>{
      this.user.editUser(params.id,this.userForm.value).subscribe(res => {
        if (res.code == 404) {
          this.toastr.error("something went wrong, please try again");
        } if (res.code == 200) {
          this.toastr.success("succesffully added");
        }
      })
    })
    

  }

}
