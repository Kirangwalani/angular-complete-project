import { EditUserComponent } from './edit-user/edit-user.component';

import { ListUsersComponent } from './list-users/list-users.component';

import { AddusersComponent } from './addusers/addusers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ListproductsComponent } from './products/listproducts/listproducts.component';
import { AddproductsComponent } from './products/addproducts/addproducts.component';
import { EditProductsComponent } from './products/edit-products/edit-products.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            // user management routes
            { path: 'addUser', component: AddusersComponent },
            { path: 'listUser', component: ListUsersComponent },
            { path: 'editUser/:id', component: EditUserComponent },
            // product maangement routes
            { path: 'addProduct', component: AddproductsComponent },
            { path: 'listProduct', component: ListproductsComponent },
            { path: 'editProduct/:id', component: EditProductsComponent },
            // blogs management routes
            { path: 'blog', loadChildren: './blogs/blogs.module#BlogsModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
