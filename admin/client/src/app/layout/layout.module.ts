import { PageHeaderModule } from './../shared/modules/page-header/page-header.module';
import { ListproductsComponent } from './products/listproducts/listproducts.component';
import { AddproductsComponent } from './products/addproducts/addproducts.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { AddusersComponent } from './addusers/addusers.component';
import { ListUsersComponent } from './list-users/list-users.component';

import { DataTableModule } from "angular-6-datatable";
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditProductsComponent } from './products/edit-products/edit-products.component';
import {TableModule} from 'primeng/table';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {PaginatorModule} from 'primeng/paginator';
import {ConfirmDialogModule} from 'primeng/confirmdialog';




@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        ReactiveFormsModule, FormsModule,
        TranslateModule,
        TableModule,
        PageHeaderModule,
        PaginatorModule,
        NgbDropdownModule.forRoot(), DataTableModule,
        ConfirmDialogModule
        // EditorModule
    ],
    declarations: [
        LayoutComponent,
        SidebarComponent,
        HeaderComponent,
        AddusersComponent,
        ListUsersComponent,
        AddproductsComponent,
        ListproductsComponent,
        EditUserComponent,
        EditProductsComponent
    
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
})
export class LayoutModule { }
