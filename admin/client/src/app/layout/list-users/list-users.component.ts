import { NotificationService } from './../../services/notification.service';
import { UsersService } from './../../services/users.service';
import { routerTransition } from './../../router.animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss'],
  animations:[routerTransition()]
})
export class ListUsersComponent implements OnInit {
usersList:any[];
user:any[]
  constructor(
    private users:UsersService,
    private route :ActivatedRoute,
    private toastr :NotificationService,
    private router:Router,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
   this.getUserList()
  }

  getUserList(){
    this.users.listUsers().subscribe(res=>{
    this.usersList =res;
    console.log(this.usersList)
    })
  }

  delete(id){
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.route.params.subscribe(params =>{
          this.users.deleteUser(id).subscribe(res =>{
           this.toastr.success("successfullt deleted");
           this.getUserList()
          })
        })
    
      },
      reject: () => {
                }
  });

    
  }
}
