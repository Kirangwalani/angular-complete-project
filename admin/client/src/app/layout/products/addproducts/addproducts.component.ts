import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { NotificationService } from './../../../services/notification.service';
import { ProductService } from './../../../services/product.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.component.html',
  styleUrls: ['./addproducts.component.scss'],
  animations: [routerTransition()]
})

export class AddproductsComponent implements OnInit {

  @ViewChild('fileInput') fileInput: ElementRef;

  productForm: FormGroup
  localUrl:any;

  constructor(private fb: FormBuilder, private product: ProductService, private toastr: NotificationService, private router: Router) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      product_name: new FormControl(''),
      category: new FormControl(''),
      price: new FormControl(''),
      product_description: new FormControl(''),
      file: null

    })
  }







  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.localUrl = (<FileReader>event.target).result;
      }

      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("file", file);
      this.productForm.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('product_name', this.productForm.get('product_name').value);
    input.append('category', this.productForm.get('category').value);
    input.append('price', this.productForm.get('price').value);
    input.append('product_description', this.productForm.get('product_description').value);
    input.append('file', this.productForm.get('file').value);
    return input;
  }

  addProduct() {
    const formModel = this.prepareSave();
    console.log("jcbnjedwnce", formModel);
    this.product.addProduct(formModel).subscribe(res => {
      if (res.err) {
        this.toastr.error("something went wrong");

      } else {
        this.toastr.success("successfully added");
        this.router.navigate(['/layout/listProduct'])
      }
    })

  }
}
