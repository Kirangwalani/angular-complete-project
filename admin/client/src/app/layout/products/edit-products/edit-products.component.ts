import { routerTransition } from './../../../router.animations';
import { ProductService } from './../../../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-edit-products',
  templateUrl: './edit-products.component.html',
  styleUrls: ['./edit-products.component.scss'],
  animations:[routerTransition()]
})
export class EditProductsComponent implements OnInit {
  productForm:FormGroup;
  productData;
  localUrl:any;
  constructor(
    private route:ActivatedRoute,
    private product:ProductService,
    private router:Router,
    private toastr:NotificationService,
    private fb:FormBuilder
  ) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      product_name: new FormControl(''),
      category: new FormControl(''),
      price: new FormControl(''),
      product_description: new FormControl(''),
      file: null

    })
    this.route.params.subscribe(params =>{
      this.product.getProductDetail(params.id).subscribe(res =>{
        this.productData = res.data;

        this.productForm.patchValue({
          product_name : this.productData.product_name, 
          category : this.productData.category,
          price : this.productData.price,
          // file:this.productData.imagePath,
          product_description : this.productData.product_description
        })
        this.localUrl =this.productData.imagePath;
      })
    })
  }


  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.localUrl = (<FileReader>event.target).result;
      }

      reader.readAsDataURL(event.target.files[0]);
      let file = event.target.files[0];
      console.log("file", file);
      this.productForm.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('product_name', this.productForm.get('product_name').value);
    input.append('category', this.productForm.get('category').value);
    input.append('price', this.productForm.get('price').value);
    input.append('product_description', this.productForm.get('product_description').value);
    input.append('file', this.productForm.get('file').value);
    return input;
  }

  onUpdateProduct(){
    const formData = this.prepareSave();
    this.route.params.subscribe(params=>{
      this.product.editProduct(params.id,formData).subscribe(res => {
        if (res.code == 404) {
          this.toastr.error("something went wrong, please try again");
        } if (res.code == 200) {
          this.toastr.success("succesffully added");
          this.router.navigate(['/layout/listProduct'])
        }
      })
    })
  

  }

}
