import { NotificationService } from './../../../services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from './../../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-listproducts',
  templateUrl: './listproducts.component.html',
  styleUrls: ['./listproducts.component.scss'],
  animations:[routerTransition()]
})
export class ListproductsComponent implements OnInit {
  cols: any[];
  products:any[];
  constructor(private product :ProductService,
    private route:ActivatedRoute,
    private toastr:NotificationService,  private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getProducts();this.getProducts();
  }
 getProducts(){
   this.product.listProduct().subscribe(res=>{
     this.products = res;

   })
   
 };

 delete(id){

  this.confirmationService.confirm({
    message: 'Are you sure that you want to Delete?',
    header: 'Confirmation',
    icon: 'pi pi-exclamation-triangle',
    accept: () => {
      this.route.params.subscribe(params =>{
        this.product.deleteProduct(id).subscribe(res =>{
          if(res.err){
            this.toastr.error("Failed to delete")
          }else{
            this.toastr.success("successfully deleted")
          }
          this.getProducts();
        })
       })
  
    },
    reject: () => {
              }
});

  
 }
}
