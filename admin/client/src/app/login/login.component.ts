import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { NotificationService } from './../services/notification.service';
import { AuthService } from './../services/auth.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    name: string
    submitted = false;
    constructor(public router: Router, private auth: AuthService, private formBuilder: FormBuilder, private toastr: NotificationService) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            "email": new FormControl('', [Validators.required, Validators.email]),
            "password": new FormControl('', [Validators.required, Validators.minLength(6)])
        })

    }
    get f() { return this.loginForm.controls; }

    onLoggedin() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.auth.onLogin(this.loginForm.value).subscribe(res => {

            if (res.code == 200) {
                // var token = res.token;
                var data = res.data;
                var name = data.name
                localStorage.setItem('token', data.token);
                localStorage.setItem('role', data.role)
                localStorage.setItem('name', name);
                localStorage.setItem('id', data._id)
                if (data.role == "admin") {
                    this.toastr.success(res.message)
                    this.router.navigate(['/layout']);
                }
                else if (data.role == "user") {
                    this.toastr.success(res.message)
                    this.router.navigate(['/users/home'])
                }

            } else if (res.code == 402) {
                this.toastr.error(res.message)
                    ;
            } else if (res.code == 401) {
                this.toastr.error(res.message)

            } else {
                this.toastr.warning("try again")
            }


        });



    }
}
