import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  base_Api_Url = environment.base_Api_Url
  constructor(private http: HttpClient, ) { }

  addBlog(data: any): Observable<any> {
    return this.http.post(this.base_Api_Url + "blog", data);
  }
  listBlog(): Observable<any> {
    return this.http.get(this.base_Api_Url + "blog");
  }
  deleteBlog(id): Observable<any> {
    return this.http.delete(this.base_Api_Url + "blog?id=" + id);
  }
  // get blog detail to edit
  getBlogDetail(id): Observable<any> {
    return this.http.get(this.base_Api_Url + "getblogDetail?id=" + id)

  }

  // edit particular blog
  editBlog(id, data: any): Observable<any> {
    return this.http.put(this.base_Api_Url + "blog?id=" + id, data);
  }


}
