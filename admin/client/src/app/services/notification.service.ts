import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr:ToastrService) { }
  success(Message:string){
    this.toastr.success(Message);
  }

  error(Message:string){
    this.toastr.error(Message);
  }

  warning(Message:string){
    this.toastr.warning(Message);
  }

  info(Message:string){
    this.toastr.info(Message);
  }
}
