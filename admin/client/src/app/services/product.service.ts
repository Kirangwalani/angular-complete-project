import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  base_Api_Url = environment.base_Api_Url
  constructor(private http: HttpClient, ) { }

  addProduct(data: any): Observable<any> {
    return this.http.post(this.base_Api_Url + "product", data);
  }
  listProduct(): Observable<any> {
    return this.http.get(this.base_Api_Url + "product");
  }
  deleteProduct(id): Observable<any> {
    return this.http.delete(this.base_Api_Url + "product?id=" + id);
  }
  // get product detail to edit
  getProductDetail(id): Observable<any> {
    return this.http.get(this.base_Api_Url + "getproductDetail?id=" + id)

  }

  // edit particular product
  editProduct(id, data: any): Observable<any> {
    return this.http.put(this.base_Api_Url + "product?id=" + id, data);
  }
}
