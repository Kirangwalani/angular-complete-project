
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  base_Api_Url = environment.base_Api_Url
  constructor(private http: HttpClient, ) { }

  addUsers(data: any): Observable<any> {
    return this.http.post(this.base_Api_Url + "user", data);
  }
  listUsers(): Observable<any> {
    return this.http.get(this.base_Api_Url + "user");
  }
  deleteUser(id): Observable<any> {
    return this.http.delete(this.base_Api_Url + "user?id=" + id);
  }
  // get user detail to edit
  getUserDetails(id): Observable<any> {
    return this.http.get(this.base_Api_Url + "getUserDetail?id=" + id)

  }

  // edit particular user
  editUser(id, data: any): Observable<any> {
    return this.http.put(this.base_Api_Url + "user?id=" + id, data);
  }
}
