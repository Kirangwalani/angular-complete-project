import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>  {
    const token =localStorage.getItem('token')
    
    // console.log(token,"token")
    // request = request.clone({
    //   headers: request.headers.set("Authorization", "Bearer " + token)
    // });
    request = request.clone({
      setHeaders: {"Authorization": "Bearer " + token}
    });

    // console.log(request,"request")
    return next.handle(request)
  }
}
