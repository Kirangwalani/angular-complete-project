import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
        if (localStorage.getItem('token')) {
            if(localStorage.getItem('role')=="admin")
            return true;
        }

        this.router.navigate(['']);
        return false;
    }
}
