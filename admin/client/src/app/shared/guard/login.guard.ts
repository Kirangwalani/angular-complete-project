import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class LoginGuard implements CanActivate {
  token = localStorage.getItem('token');
  role = localStorage.getItem("role");

  constructor(private router :Router){

  }
  canActivate(
 
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.token && this.role=="admin"){
        this.router.navigate(["/layout/dashboard"]);
        return false;    
          }
      else if(this.token && this.role=="user"){
        this.router.navigate(["/users/home"]);
        return false;
      }
      else{
        return true;
      }
    }
}
