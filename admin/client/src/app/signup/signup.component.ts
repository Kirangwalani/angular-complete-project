import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { AuthService } from './../services/auth.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    constructor(
        private auth: AuthService,
        private formBuilder: FormBuilder,
        private toastr: NotificationService,
        private router: Router
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            "name": new FormControl('', Validators.required),
            "email": new FormControl('', [Validators.required, Validators.email]),
            "password": new FormControl('', [Validators.required, Validators.minLength(6)])
        })

    }
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.auth.onSignUp(this.registerForm.value).subscribe(res => {
            if(res.code == 200){
                this.toastr.success("successfully registered");
                this.router.navigate(['']);
            }else if(res.code == 404){
                this.toastr.error(res.message);

            }else if(res.code == 400){
                this.toastr.error(res.message);

            }
            // console.log("successfully registered");
          
           
        })
    }


}
