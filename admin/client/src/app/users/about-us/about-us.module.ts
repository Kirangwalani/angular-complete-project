import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us.component';
import { AboutusroutingModule } from './aboutusrouting.module';


@NgModule({
  imports: [
    CommonModule,
    AboutusroutingModule
  ],
  declarations: [AboutUsComponent,]
})
export class AboutUsModule { }
