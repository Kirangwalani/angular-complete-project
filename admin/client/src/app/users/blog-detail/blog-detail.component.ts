import { BlogService } from './../../services/blog.service';
import { routerTransition } from './../../router.animations';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BlogDeatilService } from '../services/blog-deatil.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss'],
  animations:[routerTransition()]
})
export class BlogDetailComponent implements OnInit {
  commentList: any[]
  blogList
  userList= new Array
  commentForm:FormGroup
  rating:Number
  val: Number
  constructor(private route: ActivatedRoute, private blog: BlogDeatilService,
    private blogs:BlogService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.blogs.getBlogDetail(params.id).subscribe(res => {
        this.blogList =res.data;
  
      });
    })
  this.getcoments() 
   
  }

  getcoments() {
    this.route.params.subscribe(params => {
      this.blog.getblogDetail(params.id).subscribe(res => {
        this.commentList = res;

        // console.log(this.commentList)
        for(var i=0;i<this.commentList.length;i++){

           this.userList.push(this.commentList[i].userid);
           this.commentList[i].name = this.userList[i].name
          //  this.val= this.commentList[i].rating;
         }
        // this.blogList.push(this.commentList[0].blogid)
        // console.log(this.blogList[0]);
        // console.log(this.commentList);
    


      })

    })
  }
 
  addComment(_){
    var comment = (document.getElementById('Comment') as HTMLInputElement).value
    console.log(this.rating)
    this.route.params.subscribe(params=>{
      var data={
        'userid':localStorage.getItem('id'),
        'blogid':params.id,
        'comment':comment,
        'rating':this.rating
        
      }
      
      // console.log(data )
      this.blog.addComment(data).subscribe(res=>{
        if(res.code ==200){
          console.log("jdejdhj")
          window.location.reload();
        }
  
      })
    })

  }

}
