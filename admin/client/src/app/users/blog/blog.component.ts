import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { NotificationService } from '../../services/notification.service';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  animations:[routerTransition()]
})
export class BlogComponent implements OnInit {

  blogList: any[];
  constructor(
    private blog: BlogService,
    private toastr: NotificationService
  ) { }

  ngOnInit() {

    this.getBlogs();
  }
  getBlogs() {
    this.blog.listBlog().subscribe((res: any) => {
      this.blogList = res;
      console.log(res);

    });
  }
}


