import { NotificationService } from './../../services/notification.service';
import { Router } from '@angular/router';
import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
// import { NotificationService } from '../../services/notification.service';
// import { NotificationService } from '../../services/notification.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})



export class CartComponent implements OnInit {
  cartDetails:any;
  cities1: SelectItem[];
  quantity;
  userid = localStorage.getItem('id');
  TotalPrice=0

  constructor(
    private cart:CartService,
    private router:Router,private toastr:NotificationService
  ) {}

  ngOnInit() {
    this.getCartDetail();

  }
  productList =new Array
  cartList = new Array;
  getCartDetail(){
    this.cart.listCart(this.userid).subscribe(res =>{
     if(res.code == 200){
      this.productList = res.data;
      this.TotalPrice =Math.round(res.totalPrice)
     }else if(res.code === 400){
       console.log("error");

     }else{
     this.toastr.error("your session is expired ,please login");
       this.router.navigate['']
      //  console.log("gya kaam see")
     }
      //  console.log(res.data);
      
     

  });

}
  delete(id:any){
    var data={
      user_id:localStorage.getItem('id')
    }
console.log(data);
    this.cart.deleteProductFromCart(id,data).subscribe(res=>{
      if(res.code == 200){
      this.toastr.success("successfully deleted");
      this.getCartDetail();
      }else if(res.code == 400){
        this.toastr.error("please try again")
      }else{
        this.toastr.error("your session is expired");
        this.router.navigate([''])
      }
      // console.log("deleted");
    })
      this.getCartDetail();
    
  }

  getQuantity(product){
    this.quantity = Number((document.getElementById("quantity") as HTMLInputElement).value)
    console.log( this.quantity,"dfecvrefc");
    var data={
      user_id:localStorage.getItem('id'),
      quantity: this.quantity,
      product_id:product.product_id._id,
      productPrice:product.product_id.price
    }
    console.log(data,"dfecvrefc");

    this.cart.updateCartQuantity(data).subscribe(res=>{
      console.log("sucessfuly update",res);
      this.getCartDetail();
     
    })
    
  }

 
}
