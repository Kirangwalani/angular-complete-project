import { CheckoutService } from './../services/checkout.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { routerTransition } from './../../router.animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  animations:[routerTransition()]
})
export class CheckoutComponent implements OnInit {
  shipingForm:FormGroup;
  constructor(private checkout:CheckoutService
    ) { }

  ngOnInit() {
    this.shipingForm = new FormGroup({
      "firstname" : new FormControl('', Validators.required),
      "lastname" : new FormControl('', Validators.required),
      "contact" : new FormControl('', Validators.required),
      "email" : new FormControl('', Validators.required),
      "address_line_1" : new FormControl('', Validators.required),
      "address_line_2" : new FormControl(''),
      "country" : new FormControl('', Validators.required),
      "state" : new FormControl('', Validators.required),
      "city" : new FormControl('', Validators.required),
      "zipcode" : new FormControl('', Validators.required)
    });
  }
  addShppingDetail(){
    console.log(this.shipingForm.value);
    this.checkout.addShippingDetail(this.shipingForm.value).subscribe(res=>{
      if(res.code == 200){
        // this.
        console.log("success");

      }else[
        console.log("gfgdfgd")
      ]
    })
  }
}

