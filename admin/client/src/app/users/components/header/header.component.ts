import { CartService } from './../../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  name;
  constructor(private router:Router,private cart:CartService) { }

  ngOnInit() {
   this.name = localStorage.getItem('name');
  //  var totalNumber = this.cart.getProductNumber()
// console.log(totalNumber)
  }
  onLogout(){
    this.router.navigate([''])
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
    localStorage.removeItem('id');

  }

  

}
