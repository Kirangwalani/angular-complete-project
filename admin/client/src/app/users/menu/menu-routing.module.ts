import { MenuComponent } from './menu.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path:"",
    component:MenuComponent
  }

];

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class MenuRoutingModule { }
