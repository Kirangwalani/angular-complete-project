import { NotificationService } from './../../services/notification.service';
import { routerTransition } from './../../router.animations';
import { CartService } from './../services/cart.service';
import { ProductService } from './../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations:[routerTransition()]
})
export class MenuComponent implements OnInit {
  productList: any;
  count = 1;
  cartDetails:any;
  totalCount;
  completeData: any;
  order = new Array();
  userid = localStorage.getItem('id');
  constructor(private product: ProductService, private cart: CartService, private toastr: NotificationService, private router: Router) { }

  ngOnInit() {
    this.getProducts();
    var cart = JSON.parse(localStorage.getItem('cart'));
    console.log(cart)
    
// console.log(this.userid)
  //  this.getCartDetail()


  }
  getProducts() {
    this.product.listProduct().subscribe(res => {
      // var productChunks= [];
      // var chunkSize = 3;
      // for (var i =0; i<res.length ; i +=chunkSize){
      //   productChunks.push(  res.slice(i, i+chunkSize));

      // }
      this.productList = res
    })

  }

  addToCart(item: any) {
    var count :number = 0;
    // var quantity  = count+1;
console.log(item.price)
    var data = {
      user_id: this.userid,
      product_id: item._id,
      price :item.price,
      // quantity :quantity
    }
    console.log(data);
    this.cart.addCart(data).subscribe(res => {
      if (res.code == 401) {
        this.toastr.error("your session is expired ,please login");
        this.router.navigate(['']);
      } else if (res.code == 200) {
        this.toastr.success("successfully added to cart");
      } else {
        this.toastr.error("something went wrong")
      }
    })

  }
  // cartDetailList = new Array;
  // getCartDetail(){
  //   this.cart.listCart(this.userid).subscribe(res =>{
  //    this.cartDetails = res;
    
  //    for(var i=0; i< this.cartDetails.length; i++)
  //    this.cartDetailList.push(this.cartDetails[i].item)
  //   });
  //   console.log(this.cartDetailList) 

  // }

  

}
