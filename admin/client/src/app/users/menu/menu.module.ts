import { MenuComponent } from './menu.component';
import { MenuRoutingModule } from './menu-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import {CardModule} from 'primeng/card';
@NgModule({
  imports: [

    CommonModule
    ,MenuRoutingModule,
    MatGridListModule,
    MatListModule,
    CardModule,
    MatButtonModule  ],
  declarations: [MenuComponent]
})
export class MenuModule { }
