import { CartService } from './../services/cart.service';
import { CheckoutService } from './../services/checkout.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {
  orderSummary: any
  totalPrice
  constructor(private cart :CartService) { }

  ngOnInit() {
    this.getOrderSummary()
  }

  getOrderSummary() {
    var id = localStorage.getItem('id')
    this.cart.listCart(id).subscribe(res => {
      console.log(res);
      this.totalPrice = res.totalPrice
      this.orderSummary = res.data;
    })

  }
}
