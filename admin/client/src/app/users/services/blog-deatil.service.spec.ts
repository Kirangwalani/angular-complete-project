import { TestBed, inject } from '@angular/core/testing';

import { BlogDeatilService } from './blog-deatil.service';

describe('BlogDeatilService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogDeatilService]
    });
  });

  it('should be created', inject([BlogDeatilService], (service: BlogDeatilService) => {
    expect(service).toBeTruthy();
  }));
});
