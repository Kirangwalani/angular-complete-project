import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class BlogDeatilService {

  base_Api_Url = environment.base_Api_Url
  constructor(private http:HttpClient) { }


getblogDetail(id):Observable<any>{
return this.http.get(this.base_Api_Url +"listComment?id="+id )
// console.log(this.data,"Service side data");
  
}
addComment(data:any):Observable<any>{
  return this.http.post(this.base_Api_Url+"addComment",data)
}
}
