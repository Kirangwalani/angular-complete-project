import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CartService {
data;
base_Api_Url = environment.base_Api_Url
  constructor(private http:HttpClient) { }

addCart(order:any):Observable<any>{
return this.http.post(this.base_Api_Url +"addCart", order)
// console.log(this.data,"Service side data");
  
}

listCart(id:any):Observable<any>{
  console.log(id);
  return this.http.get(this.base_Api_Url +"listCart?id="+id)
}
deleteProductFromCart(id:any,data):Observable<any>{
  console.log(id,data);
  return this.http.get(this.base_Api_Url +"deleteProductFromCart?id="+id,data)
}
updateCartQuantity(data):Observable<any>{
  console.log(data,"ggrefref");
  return this.http.put(this.base_Api_Url +"updateCartQuantity",data)
}

}
