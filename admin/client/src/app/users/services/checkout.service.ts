import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  data;
  base_Api_Url = environment.base_Api_Url
    constructor(private http:HttpClient) { }

    addShippingDetail(data:any):Observable<any>{
      return this.http.post(this.base_Api_Url +"addShippingDetail", data)
    }

    orderSummary(id):Observable<any>{
      return this.http.get(this.base_Api_Url +"addOrder?id="+id)
    }
}


  
 
