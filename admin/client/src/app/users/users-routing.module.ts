import { PaymentComponent } from './payment/payment.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';

import { BlogComponent } from './blog/blog.component';
import { UsersComponent } from './users.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart/cart.component';


const routes :Routes=[
  {
    path:"",
    component:UsersComponent,
    children:[
      {
        path:"menu",
        loadChildren:'./menu/menu.module#MenuModule'
      },
      {
        path:"home",
        loadChildren:'./about-us/about-us.module#AboutUsModule'
      },
      {
        path:"cart",
        component:CartComponent
      },
      {
        path:"blog",
        component:BlogComponent
      },
      {
        path:"blogDetail/:id",
        component:BlogDetailComponent
      },
      {
        path:"checkout",
        component:CheckoutComponent
      },
      {
        path:"orderSummary",
        component:OrderSummaryComponent
      },
      {
        path:"payment",
        component:PaymentComponent
      }
    ]
  }

  
]

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class UsersRoutingModule { }
