import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
name;
  constructor(private router:Router) { }

  ngOnInit() {
   this.name = localStorage.getItem('name');

  }
  onLogout(){
    localStorage.removeItem('token');
    localStorage.removeItem('role');

    this.router.navigate([''])
  }

}
