import { BlogDeatilService } from './services/blog-deatil.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CartService } from './services/cart.service';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import {PickListModule} from 'primeng/picklist';
import {DropdownModule} from 'primeng/dropdown';
import { CartComponent } from './cart/cart.component';
import {CardModule} from 'primeng/card';
import { MatButtonModule } from '@angular/material/button';
import { BlogComponent } from './blog/blog.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
// import { BlogsComponent } from './blogs/blogs.component';
import {RatingModule} from 'primeng/rating';
import {SpinnerModule} from 'primeng/spinner';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { PaymentComponent } from './payment/payment.component';
// import {DropdownModule} from 'primeng/dropdown';
@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    PickListModule,
    DropdownModule,
    CardModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    RatingModule,
    SpinnerModule
  ],
  declarations: [UsersComponent, HeaderComponent, CartComponent, BlogComponent, BlogDetailComponent, CheckoutComponent, OrderSummaryComponent, PaymentComponent, ],
  providers:[CartService,BlogDeatilService]
})
export class UsersModule { }
