'use strict';
var util = require('util');
var user = require('../../schemas/userDetails');
const product=require('../../schemas/productDetails');
var blog=require('../../schemas/blogDetails');
var fs = require('fs');

module.exports = {
    userList:userList,
    deleteUser:deleteUser,
    updateUser:updateUser,
    addProduct:addProduct,
    productList:productList,
    deleteProduct:deleteProduct,
    updateProduct:updateProduct,
    addUserByAdmin:addUserByAdmin,
    deactivateUser:deactivateUser,
    addBlog:addBlog,
    blogList:blogList,
    updateBlog:updateBlog,
    deleteBlog:deleteBlog,
    setProductDetails:setProductDetails,
    getCartDetails:getCartDetails
};
var timeStamp = Date.now();

function addUserByAdmin(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
      console.log(" here",req.body);
  var record = new user();
  record.fullName = req.body.newfname;
  record.email = req.body.newemail;
  record.username = req.body.newuName;
  record.password = req.body.newpsword;
  record.role=req.body.newrole;
  record.isDeactive=req.body.newisDeactive;
  record.save(function (err, res1) {
      console.log(" Resp =", err, res1);
      if(err){
          res.json({
              data: err
          });
      }else{
          res.json({
              message: " Sucessfully added user"
          });
      }
  });
}
function userList(req, res, next) {
    var userArray = [{
      $project: {
        fullName: '$fullName',
        email: '$email',
        username: '$username',
        password: '$password',
        role: '$role',
        isDeactive:'$isDeactive'
      }
    }]
    user.aggregate(userArray, function (err, data) {
      res.json(data);
    })
  }
function deleteUser(req, res) {
    console.log("inside delete",req.swagger.params.id.value)
    user.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
      if (err) throw err;
      console.log("Deleted Successfully");
      data.save();
      res.json(data);
    })
  }
function updateUser(req, res, next) {
    var data = {
        username: req.body.newUsername,
        password: req.body.newPassword
    }
    var condition = { _id: req.swagger.params.id.value }
    console.log(condition, "==", data)
    user.update(condition, { $set: data }, function (err, data1) {
        console.log(" resp====", err, data1);
        if (err) throw err;
        // data1.save();
        console.log("Updated Successfully", data1);
        console.log("Updated data", data1);
        res.json(data1);
    });
    
}

function deactivateUser(req,res){
  var _id=req.swagger.params.id.value;
  var active_status;
  user.findById(_id,function(err,data){
    if(err){
      res.json(err);
    }else if(data){
      var status=data.isDeactive;
      active_status=(status=='Active')?'Deactive':'Active';
      user.findByIdAndUpdate(_id,{$set:{isDeactive:active_status}},function(err,data){
        if(err){
          res.json(err)
        }else{
          data.save();
          console.log("Deactivated",data);
          res.json(data);
        }
      });
    }else{
      res.json("elseeeeeeeeee");
    }
  })
}



function addProduct(req, res, next) {
  console.log('request data',req);
  var imgOriginalName = req.files.imagePath[0].originalname;
  var path = '../Backend/public/images/' + timeStamp + "_" + imgOriginalName;
  var db_path = 'http://127.0.0.1:10010/images/' + timeStamp + "_" + imgOriginalName;
  product.imagePath = path;
  console.log("Image",req.files)
  fs.writeFile(path, (req.files.imagePath[0].buffer), function (err) {
      if (err) throw err;
  })

  console.log("Inside Add Product API")

  var recordDetails = new product();

  recordDetails.productName = req.swagger.params.productName.value;
  // recordDetails.productOwner = req.body.powner;
  recordDetails.productCost = req.swagger.params.productCost.value;
  recordDetails.shippingCost = req.swagger.params.shippingCost.value;
  // recordDetails.shippingAddress = req.body.add;
  // recordDetails.phoneNumber = req.body.pnum;
  recordDetails.status = req.swagger.params.status.value;
  // recordDetails.date = req.body.date;
  recordDetails.imagePath=db_path;

  recordDetails.save(function (err, res) {
    console.log(res);
  })
  res.json("added");
}
function productList(req, res, next) {
  //res.render('list', { title: 'Express' });
  var gArray = [{
    $project: {
      productName: '$productName',
      // productOwner: '$productOwner',
      productCost: '$productCost',
      shippingCost: '$shippingCost',
      // shippingAddress: '$shippingAddress',
      // phoneNumber: '$phoneNumber',
      status: '$status',
      // date: '$date'
      imagePath:'$imagePath'
    }
  }]
  product.aggregate(gArray, function (err, data) {
    res.json(data);
  })
}
function deleteProduct(req, res) {
  console.log("inside delete",req.swagger.params.id.value)
  product.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
    if (err) throw err;
    console.log("Deleted Successfully");
    data.save();
    res.json(data);
  })
}
function updateProduct(req, res, next) {
  console.log('request data',req);
  var imgOriginalName = req.files.imagePath[0].originalname;
  var path = '../frontend/src/assets/images/' + timeStamp + "_" + imgOriginalName;
  var db_path = '/assets/images/' + timeStamp + "_" + imgOriginalName;
  product.imagePath = path;
  console.log("Image",req.files)
  fs.writeFile(path, (req.files.imagePath[0].buffer), function (err) {
      if (err) throw err;
  })
  var data = {
    productName: req.swagger.params.productName.value,
    productCost: req.swagger.params.productCost.value,
    shippingCost: req.swagger.params.shippingCost.value,
    status: req.swagger.params.status.value,
    imagePath: db_path
  }
  var condition = { _id: req.swagger.params.id.value }
  console.log(condition, "==", data)
  product.update(condition, { $set: data }, function (err, data1) {
      console.log(" resp====", err, data1);
      if (err) throw err;
      // data1.save();
      console.log("Updated Successfully", data1);
      console.log("Updated data", data1);
      res.json(data1);
  });
  
}
function addBlog(req,res){
  console.log('request data',req);
  var imgOriginalName = req.files.blogImage[0].originalname;
  var path = '../frontend/src/assets/images/' + timeStamp + "_" + imgOriginalName;
  var db_path = '/assets/images/' + timeStamp + "_" + imgOriginalName;
  blog.blogImage = path;
  console.log("Image",req.files)
  fs.writeFile(path, (req.files.blogImage[0].buffer), function (err) {
      if (err) throw err;
  })

  console.log("Inside Add Blog API")

  var recordDetails = new blog();

  recordDetails.blogTitle = req.swagger.params.blogTitle.value;
  recordDetails.des = req.swagger.params.des.value;
  recordDetails.blogImage=db_path;

  recordDetails.save(function (err, res) {
    console.log(res);
  })
  res.json("added");
}

function blogList(req, res, next) {
  //res.render('list', { title: 'Express' });
  var gArray = [{
    $project: {
      blogTitle: '$blogTitle',
      des: '$des',
      blogImage:'$blogImage'
    }
  }]
  blog.aggregate(gArray, function (err, data) {
    res.json(data);
  })
}

function updateBlog(req, res, next) {
  console.log('request data',req);
  var imgOriginalName = req.files.blogImage[0].originalname;
  var path = '../frontend/src/assets/images/' + timeStamp + "_" + imgOriginalName;
  var db_path = '/assets/images/' + timeStamp + "_" + imgOriginalName;
  product.imagePath = path;
  console.log("Image",req.files)
  fs.writeFile(path, (req.files.blogImage[0].buffer), function (err) {
      if (err) throw err;
  })
  var data = {
    blogTitle: req.swagger.params.blogTitle.value,
    des: req.swagger.params.des.value,
    blogImage: db_path
  }
  var condition = { _id: req.swagger.params.id.value }
  console.log(condition, "==", data)
  blog.update(condition, { $set: data }, function (err, data1) {
      console.log(" resp====", err, data1);
      if (err) throw err;
      // data1.save();
      console.log("Updated Successfully", data1);
      console.log("Updated data", data1);
      res.send(data1);
  });
  
}
function deleteBlog(req, res) {
  console.log("inside delete",req.swagger.params.id.value)
  blog.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
    if (err) throw err;
    console.log("Deleted Successfully");
    data.save();
    res.json(data);
  })
}

function setProductDetails(req,res){
  var _id=req.body.userid;
  user.findByIdAndUpdate(_id,{$push:{cart:{item:req.body.productid}}},function(err,data){
    if(err){
      console.log(err)
    }else{
      console.log(data)
    }
  })
}

function getCartDetails(req,res){
  var _id=req.swagger.params.id.value;
  user.findOne({_id:_id}).populate({
    path:'cart.item',
    model:'productDetails'

  }).exec(function(err,data){
    if(err){
      console.log(err);
    }else if(data){
      res.json(data.cart)
    }
    else{
      console.log("something went wrong");
    }
  })
}