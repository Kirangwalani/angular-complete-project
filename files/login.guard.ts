import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "src/app/library/service/auth.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private auth: AuthService,
    private myRoute: Router){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.auth.isLoggednIn() && localStorage.getItem("role")=="admin"){
      this.myRoute.navigate(["/admin/dashboard"]);
      return false;    
        }
    else if(this.auth.isLoggednIn() && localStorage.getItem("role")=="user"){
      this.myRoute.navigate(["/user/dashboard"]);
      return false;
    }
    else{
      return true;
    }
  }
}
