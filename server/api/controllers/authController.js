
const userModel = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');

const constantObj = require('../lib/constant');
module.exports = {
    createuser: createuser,
    userLogin: userLogin
}

function createuser(req, res) {
    let user = new userModel({
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null),
        is_active: true,
        is_delete: false,
        role: "user"
    })
    // userModel.findOne({ email: req.body.email }, function (err, data) {
    //     if (err) {
    //         res.json({
    //             code: 404,
    //             message: "your request cannot be processed",
    //             err:err
    //         })
    //     } else if (data) {
    //         res.json({
    //             code: 400,
    //             message: "Email Address already exist"
    //         })
    //     } else {
            user.save(user, function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'email id already exist'
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response
                    })
                    console.log('Successfully created');

                }

            })

    //     }
    // })

}

function userLogin(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    userModel.findOne({
        email: req.body.email,
        // is_delete: false
    }, function (err, admin) {
        if (err || !admin) {
            res.json({
                code: 402,
                message: "Email or password not found"
            })
        } else if (bcrypt.compareSync(password, admin.password)) {
            if (admin.is_active === false) {
                res.json({
                    code: 401,
                    message: "Your acccoutn is Inactive"
                })
            }
            else {
                var expirationDuration = 60 * 60 * 8 * 1; // expiration duration 8 Hours

                jwtToken = jwt.sign(admin.toJSON(), 'kiran', {
                    expiresIn: expirationDuration
                });
                userModel.update({
                    _id: admin._id
                }, {
                        $set: {
                            token: jwtToken,
                        }
                    }, function (err, data) {
                        if (err) {
                            res.json({
                                code: 401,
                                message: "Request could not be processed"
                            })
                        } else {
                            console.log(data)
                            var data = {
                                role: admin.role,
                                token: jwtToken,
                                name: admin.name,
                                _id : admin._id
                            };
                            res.json({
                                code: 200,
                                message: "You have successfully Logged In ",
                                data: data
                            })
                        }
                    });
            }
        } else {
            res.json({
                code: 401,
                message: "Invalid user or password !"
            })
        }
    });

}


