var fs = require('fs');
const blogModel = require('../models/blogs');
module.exports = {
    addBlog: addBlog,
    listBlog: listBlog,
    getBlogDetail:getBlogDetail,
    deleteBlog:deleteBlog,
    editBlog:editBlog

}

function addBlog(req, res) {
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    orignalImageName =  timeStamp + "_" +req.files.file[0].originalname;
    var imagePath = '../server/public/blogImages/'+ orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })
    var record = new blogModel();
    record.title = req.swagger.params.title.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    record.description = req.swagger.params.description.value;
    record.about = req.swagger.params.about.value;
    record.is_delete = false
    record.date = Date.now()
    record.blogImage ="http://172.10.21.161:3000/blogImages/"+orignalImageName
    record.save(function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'Blog not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}

function listBlog(req, res) {
    blogModel.find({ is_delete: false }, function (err, data) {
        console.log("After Listing data", data);
        res.json(data);
    });
}
// api to get particular product details
function getBlogDetail(req, res) {
    var _id = req.swagger.params.id.value
    blogModel.findById(_id, function (err, productData) {
        console.log("dewfdcewd", _id);
        if (err) {
            res.json({
                message: "error"
            })
        } else if (productData) {
            res.json({
                message: "success",
                data: productData
            })
        }
        else {
            res.json({
                message: "something went wrong",

            })
        }
    })
}


function deleteBlog(req, res) {

    let _id = req.swagger.params.id.value
    let is_delete =true
    console.log(_id);
    blogModel.findByIdAndUpdate(_id,{$set: {is_delete :is_delete}}, function (err, data) {

        if (err) {
            res.json(err);
        } else {
            data.save()
            res.json({code:200,data:data});
        }
      
    });
};

// Edit Users.
function editBlog(req, res) {

    
    let _id = req.swagger.params.id.value

    timeStamp = Date.now();
    orignalImageName =  timeStamp + "_" +req.files.file[0].originalname;
    var imagePath = '../admin/client/src/assets/images/blogUploads/'+ orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })
  
   var title = req.swagger.params.title.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
   var description = req.swagger.params.description.value;
   var blogImage ="assets/images/blogUploads/"+orignalImageName

    blogModel.findByIdAndUpdate(_id, { $set: {title:title, description: description,blogImage:blogImage } }, function (err, data) {
        console.log("data", _id)
        if (err) {
            console.log("errr", err)
        } else {
            data.save();
            console.log("data", data)
        }
       
        res.json({ data:data, code: 200 });
    });
};




