// import { element } from 'protractor';
const cartModel = require('../models/cart');
const productModel = require('../models/products');
const userModel = require('../models/user');
module.exports = {
    addCart: addCart,
    listCart: listCart,
    deleteProductFromCart: deleteProductFromCart,
    updateCartQuantity:updateCartQuantity
}
function addCart(req, res) {
    var record = new cartModel();
            record.product_id = req.body.product_id;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
            record.date = Date.now();
            record.user_id = req.body.user_id;
            record.productPrice = req.body.price;
            record.quantity = 1;
            record.is_delete = false;

    cartModel.findOne({ product_id: req.body.product_id ,user_id:record.user_id,is_delete:false}, function (err, result) {
        if (err) {
            consol.log(err);
        } else if (result) {
            console.log(result);
            quant = result.quantity+1

            totalPrice =   quant * record.productPrice;
            cartModel.findOneAndUpdate({product_id :req.body.product_id,user_id:record.user_id,is_delete:false},{$set:{productPrice:totalPrice, quantity:quant}}, function(err, result){
                if(err){
                    console.log(err);
                }else{
                    res.json({
                        code:200,
                        message:"success",
                        data:result
                    })
                }
            })
        }
        else {
            record.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'comment not Added',
                        // data:response
                    })
                }  else {
                            res.json({
                                code: 200,
                                data: response
                            })

                        }
            })

        }
    })

}
productList = new Array;
// cartdata;
function listCart(req, res) {
    cartModel.find({ user_id: req.swagger.params.id.value, is_delete: false }).sort({ date: 'descending' }).populate({
        path: 'product_id',
        model: 'productModel',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: "please try again"
            })

        } else if (data) {
            totalPrice = 0;
            for(var i=0; i<data.length;i++){
                totalPrice = totalPrice + data[i].productPrice

            }
            res.json({
                code : 200,
                message : "Successful fetch data",
                data : data,
                totalPrice : totalPrice

            })
        }


    })

}

function deleteProductFromCart(req, res) {
 var del = true;
//  var user_id =req..user_id;
    var product_id = req.swagger.params.id.value;
    // console.log(user_id)
    cartModel.findOneAndRemove({product_id: product_id ,is_delete :false}, function (err, data) {
        if (err) {
            res.json({
                code: 400,
                message: "product not deleted from cart",
                // err:err
            })
        } else if(data)  {
            res.json({
                code: 200,
                message: "succssfully deleted"
            })
        }
    })
}

function updateCartQuantity(req,res){
     quant = req.body.quantity;
    totalPrice= quant * req.body.productPrice;
    product_id = req.body.product_id;
    console.log(product_id)
    cartModel.findOneAndUpdate({product_id:product_id,is_delete:false},{$set:{quantity:quant,productPrice:totalPrice}},function(err, data){
        if(err){
            res.json({
                code:400,
                message:"quantity nnot updated",
                err:err
            })
        }else if(data){
            res.json({
                code:200,
                message:"successfully added"
            })

        }
    })

}



