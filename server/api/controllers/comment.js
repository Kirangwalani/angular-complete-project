// var fs = require('fs');
const commentModel = require('../models/comment');
module.exports = {
    addComment: addComment,
    listComment: listComment
}

function addComment(req, res) {

    var record = new commentModel();
    record.comment = req.body.comment;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    record.date = Date.now();
    record.blogid = req.body.blogid;
    record.rating=req.body.rating;
    record.is_delete = false;
    record.userid = req.body.userid;
    record.save(function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'comment not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}

function listComment(req, res) {
    commentModel.find({ blogid: req.swagger.params.id.value, is_delete: false }).sort({ date: 'descending' }).populate({
        path: 'blogid',
        model: 'blogModel',
    }).populate({
        path: 'userid',
        model: 'UserModel',

    }).exec(function (err, data) {
        console.log("After Listing data", data);
        res.json(data);
    })
}
// // api to get particular product details
// function getBlogDetail(req, res) {
//     var _id = req.swagger.params.id.value
//     blogModel.findById(_id, function (err, productData) {
//         console.log("dewfdcewd", _id);
//         if (err) {
//             res.json({
//                 message: "error"
//             })
//         } else if (productData) {
//             res.json({
//                 message: "success",
//                 data: productData
//             })
//         }
//         else {
//             res.json({
//                 message: "something went wrong",

//             })
//         }
//     })
// }










