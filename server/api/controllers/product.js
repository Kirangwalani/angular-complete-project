var fs = require('fs');
const productModel = require('../models/products');
const userModel = require('../models/user');
module.exports = {
    addproduct: addproduct,
    listproduct: listproduct,
    getProductDetail: getProductDetail,
    deleteProduct: deleteProduct,
    editProduct: editProduct,
   

}

function addproduct(req, res) {
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../admin/client/src/assets/images/productUploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })
    var record = new productModel();
    record.product_name = req.swagger.params.product_name.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    record.category = req.swagger.params.category.value;
    record.price = req.swagger.params.price.value;
    record.product_description = req.swagger.params.product_description.value;
    record.is_delete = false
    record.imagePath = "assets/images/productUploads/" + orignalImageName
    record.save(function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}

function listproduct(req, res) {
    productModel.find({ is_delete: false }, function (err, data) {
        console.log("After Listing data", data);
        res.json(data);
    });
}
// api to get particular product details
function getProductDetail(req, res) {
    var _id = req.swagger.params.id.value
    productModel.findById(_id, function (err, productData) {
        console.log("dewfdcewd", _id);
        if (err) {
            res.json({
                message: "error"
            })
        } else if (productData) {
            res.json({
                message: "success",
                data: productData
            })
        }
        else {
            res.json({
                message: "something went wrong",

            })
        }
    })
}


function deleteProduct(req, res) {
    let _id = req.swagger.params.id.value
    let is_delete = true
    console.log(_id);
    productModel.findByIdAndUpdate(_id, { $set: { is_delete: is_delete } }, function (err, data) {

        if (err) {
            res.json(err);
        } else {
            data.save()
            res.json(data);
        }

    });
};

// Edit Users.
function editProduct(req, res) {



    let _id = req.swagger.params.id.value
    console.log(_id);
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../admin/client/src/assets/images/productUploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var product_name = req.swagger.params.product_name.value;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    var category = req.swagger.params.category.value;
    var price = req.swagger.params.price.value;
    var product_description = req.swagger.params.product_description.value
    var imagePath = "assets/images/productUploads/" + orignalImageName

    productModel.findByIdAndUpdate(_id, { $set: { product_name: product_name, category: category, price: price, product_description: product_description, imagePath: imagePath } }, function (err, data) {
        console.log("data", _id)
        if (err) {
            console.log("errr", err)
        } else {
            data.save();
            console.log("data", data)
        }

        res.json({ data: data, code: 200 });
    });
};







