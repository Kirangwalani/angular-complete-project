// import { element } from 'protractor';
const shippingModel = require('../models/shipping');
const productModel = require('../models/products');
const userModel = require('../models/user');
module.exports = {
    addShippingDetail: addShippingDetail,

}
function addShippingDetail(req, res) {
    var record = new shippingModel()
    record.user_id =  req.body.userid;
    record.firstname = req.body.firstname;
    record.lastname = req.body.lastname;
    record.email = req.body.email;
    record.contact = req.body.contact;
    record.address_line_1 = req.body.address_line_1;
    record.address_line_2 = req.body.address_line_2;
    record.state = req.body.state;
    record.country = req.body.country;
    record.city = req.body.city;
    record.zipcode = req.body.zipcode;

    record.save( function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'Address not Added'
            })
        } else if(response){
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }else{
            res.json({
                code: 401,
                message:'Unauthorized user'
            })
        }

    })


}







