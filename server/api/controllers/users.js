
const userModel = require('../models/user');

module.exports = {
    listUser: listUser,
    deleteUser: deleteUser,
    editUser: editUser,
    addUser: addUser,
    getUserDetail: getUserDetail

}

// adduser
function addUser(req, res) {
    let user = new userModel({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        is_active: true,
        is_delete: false,
        role: "user"
    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}
//  List Users. 
function listUser(req, res) {
    userModel.find({is_delete :false},function (err, data) {
        console.log("List", data)
        res.json(data);
    });
}

//Get User by ID
function getUserDetail(req, res) {
    var _id = req.swagger.params.id.value
    userModel.findById(_id, function (err, userData) {
        console.log("dewfdcewd", _id);
        if (err) {
            res.json({
                message: "error"
            })
        } else if (userData) {
            res.json({
                message: "success",
                data: userData
            })
        }
        else {
            res.json({
                message: "something went wrong",

            })
        }
    })
}

// Delete Users. 
function deleteUser(req, res) {
    let _id = req.swagger.params.id.value
    let is_delete =true
    console.log(_id);
    userModel.findByIdAndUpdate(_id,{$set: {is_delete :is_delete}}, function (err, data) {

        if (err) {
            console.log("errr", err)
        } else {
            console.log("data", data)
        }
    
        data.save();
        res.json(data);
    });
};

// Edit Users.
function editUser(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password
    var is_active = req.body.is_active;
    var role = req.body.role;

    userModel.findByIdAndUpdate(_id, { $set: { name: name, email: email, password: password, role: role } }, function (err, data) {
        if (err) {
            console.log("errr", err)
        } else {
            console.log("data", data)
        }
        // data.save();
        res.json({ data: data, code: 200 });
    });
};