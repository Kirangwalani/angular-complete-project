'use strict';


    var jwt = require('jsonwebtoken'),
    //constant = require('./../../constants'),
    constant = require('./constant'),
    mongoose = require('mongoose'),
    user = require('../models/user');
module.exports = {
    ensureAuthorized: ensureAuthorized
} 

function ensureAuthorized(req, res, next) {

    // console.log(req.headers,"header")
    var unauthorizedJson = {code: 401, 'message': 'Unauthorized', data: {}};
    var token = req.headers["authorization"];
    // console.log(token,'uss side token')
    //if (req.headers.authorization) {
        if (token != null) {
            //var token = req.headers.authorization;
            var splitToken = token.split(' ');
            try {
                
                token = splitToken[1];
                // console.log(token,'is side token')
            var decoded = jwt.verify(token, 'kiran');
            if(decoded){
                req.user = decoded;
                // console.log(decoded);
                if (splitToken[0] == 'Bearer') {
                    user.findOne({ _id: decoded._id, }).exec(function(err, user) {
                        // console.log(user,'user')
                        if (err || !user) {
                            return res.json(unauthorizedJson);
                        } else {
                            req.user = user;
                            next();
                        }
                    });
                } else {
                    return res.json(unauthorizedJson);
                }
            }else{
                return res.json(unauthorizedJson);

            }
            
        } catch (err) {
            console.log(err)
            return res.json(unauthorizedJson);
        }
    } else {
        return res.json(unauthorizedJson);
    }
}