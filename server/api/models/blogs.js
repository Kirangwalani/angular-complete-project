const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let BlogSchema = new mongoose.Schema({
    title: { type: String },
    about: {type: String},
    description: { type: String },
    is_delete : {type: Boolean},
    blogImage: { type: String },
    date: {type:Date}

   
 
});
const blogModel = mongoose.model('blogModel', BlogSchema);
module.exports = blogModel;
