const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let CartSchema = new mongoose.Schema({
    product_id: {
        type: String,
        ref: 'productModel'},

    user_id:{
        type:String,
        ref: 'UserModel'},
        
    quantity: { 
              type: Number },
    productPrice : {
            type: Number
            },
    is_delete: {
            type: Boolean
            },
    date: {
        type:Date
         }

   
 
});
const cartModel = mongoose.model('cartModel', CartSchema);
module.exports = cartModel;
