const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let commentSchema = new mongoose.Schema({
    userid: { type: String, ref:'UserModel' },
    comment: { type: String },
    blogid : { type: String, ref:'blogModel'},
    date: { type: Date },
    reply: { type: String },
    rating: {type: Number},
    is_delete : {type: Boolean}

   
 
});
const commentModel = mongoose.model('commentModel', commentSchema);
module.exports = commentModel;
