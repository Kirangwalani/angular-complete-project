const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let orderSchema = new mongoose.Schema({
    finalCart: {
        type: Array
    },

    totalPrice: {
        type: Number
    },
    shipingCharge: {
        type: Number
    },
    user_id: {
        type: String

    },

    date:
    {
        type: Date
    }




});
const orderModel = mongoose.model('orderModel', orderSchema);
module.exports = orderModel;
