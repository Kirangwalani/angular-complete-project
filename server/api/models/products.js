const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ProductSchema = new mongoose.Schema({
    product_name: { type: String },
    category: { type: String },
    price: { type: Number },
    product_description: { type: String },
    is_delete : {type: Boolean},
    imagePath : {type: String}

   
 
});
const productModel = mongoose.model('productModel', ProductSchema);
module.exports = productModel;
