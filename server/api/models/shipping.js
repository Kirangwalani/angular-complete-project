const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ShipingSchema = new mongoose.Schema({
    user_id: {
        type: String,
        ref: 'UserModel'
    },

    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        required: true
    },
    contact: {
        type: Number,
        required: true
    },
    address_line_1: {
        type: String,
        required: true
    },
    address_line_2: {
        type: String,
        default: ''
    },
    state: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    city: {
        type: String,
        default: ''
    },
    zipcode: {
        type: Number,
        required: true
    },



});
const shippingModel = mongoose.model('shippingModel', ShipingSchema);
module.exports = shippingModel;
