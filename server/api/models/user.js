const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let UserSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String, unique: true, lowercase: true  },
    password: { type: String },
    is_active: { type: Boolean },
    is_delete: { type: Boolean },
    role: { type: String },
    token: { type: String },


});
const userModel = mongoose.model('UserModel', UserSchema);
module.exports = userModel;
