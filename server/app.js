'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var mongoose = require('mongoose');
var express = require('express');
var path= require('path');
const multer = require('multer');
var utils = require('./api/lib/utils');
const bodyParser = require('body-parser')
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
// cors configure
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE,OPTIONS');
  next();
});


//Check to call web services where token is not required//
app.use('/api/*', function(req, res, next) {
  var freeAuthPath = [
  // '/api/userRegister',
  '/api/userLogin',
  // '/api/forgotPassword',
  // '/api/adminForgotPassword',
  '/api/createuser',
  ];
  var available = false;
  for (var i = 0; i < freeAuthPath.length; i++) {
  if (freeAuthPath[i] == req.baseUrl) {
  available = true;
  break;
  }
  }
  if (!available) {
  utils.ensureAuthorized(req, res, next);
  } else {
  next();
  }
  });

//DB Connection

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/sublime');

mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('Database connection successful!');
});
app.use(express.static(path.join(__dirname, 'public')));
SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  // config swagger ui

  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  var port = process.env.PORT || 3000;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://172.10.21.161:' + port + '/hello?name=Scott');
  }
});
